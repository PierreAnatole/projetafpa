package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ActiviteDaoImpl;
import entities.Activite;
import entities.ActiviteRepository;

/**
 * Servlet implementation class ServletRefresh
 */
@WebServlet("/ServletRefresh")
public class ServletRefresh extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletRefresh() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ActiviteDaoImpl dao = new ActiviteDaoImpl();
		ArrayList<Activite> activites = dao.getAllActivite();
		
		ActiviteRepository repo = new ActiviteRepository();
		repo.setAllActivite(activites);
		
		
		request.setAttribute("repo", repo);
		this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
