package web;

import java.io.IOException;
import java.time.LocalTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ActiviteDaoImpl;
import entities.Activite;
import util.UtilHeure;

/**
 * Servlet implementation class ServletAjout
 */
@WebServlet("/ServletAjout")
public class ServletAjout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAjout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String debutStr = (String)request.getParameter("debut");
		String finStr = (String)request.getParameter("fin");
		
		LocalTime debut = UtilHeure.stringToLocalDateTime(debutStr);
		LocalTime fin = UtilHeure.stringToLocalDateTime(finStr);
				
		if(debut.compareTo(fin)!=-1) {//si l'heure de debut n'est pas strictement inferieure a l'heure de fin
			request.setAttribute("errDate","l'heure de fin doit �tre strictement superieur a l'heure de debut");
			response.sendRedirect("/projetEdt/ServletRefresh");
		}
		
		String nom = (String)request.getParameter("matiere");
		String jour = (String)request.getParameter("jour");
		
		Activite activite = new Activite(nom, jour, debut, fin);
		ActiviteDaoImpl dao = new ActiviteDaoImpl();
		dao.addActivite(activite);
		response.sendRedirect("/projetEdt/ServletRefresh");
	}

}
