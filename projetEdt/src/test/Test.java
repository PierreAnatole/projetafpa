package test;

import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import dao.ActiviteDaoImpl;
import dao.HeureDaoImpl;
import entities.Activite;
import entities.ActiviteRepository;

public class Test {
public static void main(String[] args) {
		ActiviteDaoImpl dao = new ActiviteDaoImpl();
		ArrayList<Activite> activites = dao.getAllActivite();
		
		ActiviteRepository repo = new ActiviteRepository();
		repo.setAllActivite(activites);
		
		
		
		System.out.println(repo.getActivite("lundi", 18,30));
		

		Time t = new Time(9, 30, 0);
		LocalTime lt = t.toLocalTime();
		
		HashMap<String, HashMap<LocalTime,String>> tableau = repo.getTableau();
		System.out.println(tableau.get("lundi"));
	}
}
