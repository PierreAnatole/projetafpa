package entities;

import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public class ActiviteRepository {
	private HashMap<String, HashMap<LocalTime,String>> tableau = new HashMap<String, HashMap<LocalTime,String>>();
	
	public ActiviteRepository () {
		String[] jours = {"lundi","mardi","mercredi","jeudi","vendredi"};
		
		for (int i = 0; i < jours.length; i++) {
			
			@SuppressWarnings("deprecation")
			Time t = new Time(8, 30, 0);
			LocalTime lt = t.toLocalTime();
			HashMap<LocalTime,String> heures = new HashMap<LocalTime,String>();
			for (int j = 0; j < 22; j++) {
				heures.put(lt, "");
				lt=lt.plusMinutes(30);
			}
			tableau.put(jours[i], heures);
		}
	}
	
	public String getActivite(String jour, LocalTime time) {
		return tableau.get(jour).get(time);
	}
	
	public String getActivite(String jour, int heure, int minute) {
		@SuppressWarnings("deprecation")
		Time t = new Time(heure, minute, 0);
		LocalTime lt = t.toLocalTime();
		return getActivite(jour, lt);
	}
	
	public void setActivite(String jour, LocalTime time, String activite) {
		tableau.get(jour).put(time, activite);
	}
	
	public void setAllActivite(ArrayList<Activite> activites) {
		
		for (Activite activite : activites) {
			LocalTime hd = activite.getHeureDebut();
			LocalTime hf = activite.getHeureFin();
			while(hd.isBefore(hf)) {
				setActivite(activite.getJour(), hd, activite.getNom());
				hd=hd.plusMinutes(30);
			}
		}
	}

	public HashMap<String, HashMap<LocalTime, String>> getTableau() {
		return tableau;
	}
	
	
}
