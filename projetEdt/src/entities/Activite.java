package entities;

import java.time.LocalTime;

public class Activite {
	private String nom;
	private String jour;
	private LocalTime heureDebut;
	private LocalTime heureFin;
	
	/**
	 * @param nom nom de la matiere
	 * @param nomJour nom du jour pour cette activite
	 * @param heureDebut heure de debut de l'activite
	 * @param heureFin heure de fin de l'activite
	 */
	public Activite(String nom, String jour, LocalTime heureDebut, LocalTime heureFin) {
		this.nom=nom;
		this.jour=jour;
		this.heureDebut=heureDebut;
		this.heureFin=heureFin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public LocalTime getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(LocalTime heureDebut) {
		this.heureDebut = heureDebut;
	}

	public LocalTime getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(LocalTime heureFin) {
		this.heureFin = heureFin;
	}

	
}
