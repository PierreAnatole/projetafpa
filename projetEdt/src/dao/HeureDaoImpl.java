package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.HashMap;

public class HeureDaoImpl {
	ConnexionPostgres cnx = ConnexionPostgres.getInstance();
	Connection dba = cnx.getCnx();
	
	public HashMap<LocalTime, Integer> getMapId() {
		String query = "select id_heure,nom_heure from heure;";
		HashMap<LocalTime, Integer> map= new HashMap<LocalTime, Integer>();
		try {
			PreparedStatement stmt = dba.prepareStatement(query);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				int idHeure = res.getInt("id_heure");
				Time nomHeure = res.getTime("nom_heure");
				map.put(nomHeure.toLocalTime(), idHeure);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * renvoie l'id d'une heure a partir de sa valeur
	 * @param nomJour la valeur de l'heure
	 * @return l'id de l'heure si elle existe
	 */
	public int getIdFromHeure(LocalTime lt) {
			HashMap<LocalTime, Integer> map = getMapId();
			return map.get(lt);
	}
}
