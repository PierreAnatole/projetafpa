package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JourDaoImpl {
	ConnexionPostgres cnx = ConnexionPostgres.getInstance();
	Connection dba = cnx.getCnx();
	
	/**
	 * renvoie l'id d'un jour a partir de son nom
	 * @param nomJour le nom du jour
	 * @return l'id du jour si il existe, sinon renvoie -1
	 */
	public int getIdFromNom(String nomJour) {
		String query ="select id_jour from jour where nom_jour=?;";
		try {
			PreparedStatement stmt = dba.prepareStatement(query);
			stmt.setString(1, nomJour);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				int idJour = res.getInt("id_jour");
				return idJour;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
