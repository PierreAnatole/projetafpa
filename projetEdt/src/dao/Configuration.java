package dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class Configuration {
	private String port;
	private String username;
	private String password;
	private String dbName;
	
	public Configuration () {
		Properties prop = new Properties();
		
		try {
			InputStream inp = Configuration.class.getResourceAsStream("config.properties");//recupere le fichier de configurations
			
			if(inp == null) {
				System.out.println("pas de fichiers");
			}
			
			prop.load(inp);//envoie les donnees du fichier dans l'objet Properties
			
			this.port=prop.getProperty(Objects.toString("jdbc.port",""));//initialise le port de l'objet Configuration avec le port dans le fichier(ou chaine vide si il est null)
			this.username=prop.getProperty("jdbc.username");
			this.password=prop.getProperty("jdbc.password");
			this.dbName=prop.getProperty("jdbc.dbName");
			
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public String getPort() {
		return port;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getDbName() {
		return dbName;
	}
	
	public static void main(String[] args) {
		Configuration conf = new Configuration();
		System.out.println(conf.getDbName());
	}
}
