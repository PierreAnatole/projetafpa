package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;

import entities.Activite;

public class ActiviteDaoImpl {
	ConnexionPostgres cnx = ConnexionPostgres.getInstance();
	Connection dba = cnx.getCnx();
	
	public ArrayList<Activite> getAllActivite() {
		String query =
				"select nom,nom_jour,h1.nom_heure as \"heure_debut\",h2.nom_heure as \"heure_fin\" from activite " + 
				"INNER JOIN jour ON activite.id_jour=jour.id_jour " + 
				"INNER JOIN heure as h1 ON activite.id_heure_debut=h1.id_heure " + 
				"INNER JOIN heure as h2 ON activite.id_heure_fin=h2.id_heure;";
		ArrayList<Activite> activites = new ArrayList<Activite>();
		try {
			PreparedStatement stmt = dba.prepareStatement(query);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				String nom=res.getString("nom");
				String nomJour=res.getString("nom_jour");
				Time heureDebut=res.getTime("heure_debut");
				LocalTime heureDebutLocal = heureDebut.toLocalTime();
				Time heureFin=res.getTime("heure_fin");
				LocalTime heureFinLocal = heureFin.toLocalTime();
				
				Activite activite = new Activite(nom, nomJour, heureDebutLocal, heureFinLocal);
				activites.add(activite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return activites;
	}
	
	public void addActivite(Activite activite) {
		String query = "INSERT INTO activite(nom,id_jour,id_heure_debut,id_heure_fin) VALUES (?,?,?,?);";
		try {
			PreparedStatement stmt = dba.prepareStatement(query);
			stmt.setString(1, activite.getNom());
			
			JourDaoImpl jdao = new JourDaoImpl();
			stmt.setInt(2, jdao.getIdFromNom(activite.getJour()));
			
			HeureDaoImpl hDaoImpl = new HeureDaoImpl();
			stmt.setInt(3, hDaoImpl.getIdFromHeure(activite.getHeureDebut()));
			stmt.setInt(4, hDaoImpl.getIdFromHeure(activite.getHeureFin()));
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteAll() {
		String query = "DELETE FROM activite";
		try {
			PreparedStatement stmt = dba.prepareStatement(query);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
