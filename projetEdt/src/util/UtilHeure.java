package util;

import java.sql.Time;
import java.time.LocalTime;

public class UtilHeure {
	
	public static LocalTime stringToLocalDateTime(String heure) {
		String[] horaireStr = heure.split(":");
		String heureStr=horaireStr[0];
		String minuteStr=horaireStr[1];
		
		@SuppressWarnings("deprecation")
		Time t = new Time(Integer.parseInt(heureStr),Integer.parseInt(minuteStr),0);
		LocalTime lt = t.toLocalTime();
		return lt;
	}

}
