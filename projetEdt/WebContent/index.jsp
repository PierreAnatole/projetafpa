<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/index.css"/>
<title>Emploi du temps</title>
</head>
<body>

<form action="ServletAjout" method="post" id="form">

    <div><label for="jour">jour</label>
        <select name="jour" id="jour">
            <option value="lundi">lundi</option>
            <option value="mardi">mardi</option>
            <option value="mercredi">mercredi</option>
            <option value="jeudi">jeudi</option>
            <option value="vendredi">vendredi</option>
        </select>
    </div>

    <div><label for="debut">heure de debut</label>
        <select name="debut" id="debut">
            <option value="8:30">8:30</option>
            <option value="9:00">9:00</option>
            <option value="9:30">9:30</option>
            <option value="10:00">10:00</option>
            <option value="10:30">10:30</option>           
            <option value="11:00">11:00</option>
            <option value="11:30">11:30</option>
            <option value="12:00">12:00</option>
            <option value="12:30">12:30</option>
            <option value="13:00">13:00</option>
            <option value="13:30">13:30</option>
            <option value="14:00">14:00</option>
            <option value="14:30">14:30</option>
            <option value="15:00">15:00</option>
            <option value="15:30">15:30</option>
            <option value="16:00">16:00</option>
            <option value="16:30">16:30</option>
            <option value="17:00">17:00</option>
            <option value="17:30">17:30</option>
            <option value="18:00">18:00</option>
            <option value="18:30">18:30</option>
        </select>
    </div>

    <div><label for="fin">heure de fin</label>
        <select name="fin" id="fin">
            <option value="9:00">9:00</option>
            <option value="9:30">9:30</option>
            <option value="10:00">10:00</option>
            <option value="10:30">10:30</option>           
            <option value="11:00">11:00</option>
            <option value="11:30">11:30</option>
            <option value="12:00">12:00</option>
            <option value="12:30">12:30</option>
            <option value="13:00">13:00</option>
            <option value="13:30">13:30</option>
            <option value="14:00">14:00</option>
            <option value="14:30">14:30</option>
            <option value="15:00">15:00</option>
            <option value="15:30">15:30</option>
            <option value="16:00">16:00</option>
            <option value="16:30">16:30</option>
            <option value="17:00">17:00</option>
            <option value="17:30">17:30</option>
            <option value="18:00">18:00</option>
            <option value="18:30">18:30</option>
            <option value="19:00">19:00</option>
        </select>
    </div>

    <div>
        <label for="matiere">matiere</label>
        <input type="text" name="matiere" id="matiere">    
    </div>

    
    <div>${errDate}</div>
    
</form>
<form action="ServletDelete" method="post" id="formdel"></form>

<div class="action">
    <button type="submit" form="form">ajouter</button>
    <button type="submit" form="formdel">effacer le tableau</button>
</div>


<table>
	<tr>
		<th></th>
		<th>lundi</th>
		<th>mardi</th>
		<th>mercredi</th>
		<th>jeudi</th>
		<th>vendredi</th>
 	</tr>
	
	<tr>
		<td>8:30</td>
		<td>${repo.getActivite("lundi", 8,30)}</td>
		<td>${repo.getActivite("mardi", 8,30)}</td>
		<td>${repo.getActivite("mercredi", 8,30)}</td>
		<td>${repo.getActivite("jeudi", 8,30)}</td>
		<td>${repo.getActivite("vendredi", 8,30)}</td>
	</tr>
	<tr>
		<td>9:00</td>
		<td>${repo.getActivite("lundi", 9,00)}</td>
		<td>${repo.getActivite("mardi", 9,00)}</td>
		<td>${repo.getActivite("mercredi", 9,00)}</td>
		<td>${repo.getActivite("jeudi", 9,00)}</td>
		<td>${repo.getActivite("vendredi", 9,00)}</td>
	</tr>
	<tr>
		<td>9:30</td>
		<td>${repo.getActivite("lundi", 9,30)}</td>
		<td>${repo.getActivite("mardi", 9,30)}</td>
		<td>${repo.getActivite("mercredi", 9,30)}</td>
		<td>${repo.getActivite("jeudi", 9,30)}</td>
		<td>${repo.getActivite("vendredi", 9,30)}</td>
	</tr>
	<tr>
		<td>10:00</td>
		<td>${repo.getActivite("lundi", 10,00)}</td>
		<td>${repo.getActivite("mardi", 10,00)}</td>
		<td>${repo.getActivite("mercredi", 10,00)}</td>
		<td>${repo.getActivite("jeudi", 10,00)}</td>
		<td>${repo.getActivite("vendredi", 10,00)}</td>
	</tr>
	<tr>
		<td>10:30</td>
		<td>${repo.getActivite("lundi", 10,30)}</td>
		<td>${repo.getActivite("mardi", 10,30)}</td>
		<td>${repo.getActivite("mercredi", 10,30)}</td>
		<td>${repo.getActivite("jeudi", 10,30)}</td>
		<td>${repo.getActivite("vendredi", 10,30)}</td>
	</tr>
	<tr>
		<td>11:00</td>
		<td>${repo.getActivite("lundi", 11,00)}</td>
		<td>${repo.getActivite("mardi", 11,00)}</td>
		<td>${repo.getActivite("mercredi", 11,00)}</td>
		<td>${repo.getActivite("jeudi", 11,00)}</td>
		<td>${repo.getActivite("vendredi", 11,00)}</td>
	</tr>
	<tr>
		<td>11:30</td>
		<td>${repo.getActivite("lundi", 11,30)}</td>
		<td>${repo.getActivite("mardi", 11,30)}</td>
		<td>${repo.getActivite("mercredi", 11,30)}</td>
		<td>${repo.getActivite("jeudi", 11,30)}</td>
		<td>${repo.getActivite("vendredi", 11,30)}</td>
	</tr>
	<tr>
		<td>12:00</td>
		<td>${repo.getActivite("lundi", 12,00)}</td>
		<td>${repo.getActivite("mardi", 12,00)}</td>
		<td>${repo.getActivite("mercredi", 12,00)}</td>
		<td>${repo.getActivite("jeudi", 12,00)}</td>
		<td>${repo.getActivite("vendredi", 12,00)}</td>
	</tr>
	<tr>
		<td>12:30</td>
		<td>${repo.getActivite("lundi", 12,30)}</td>
		<td>${repo.getActivite("mardi", 12,30)}</td>
		<td>${repo.getActivite("mercredi", 12,30)}</td>
		<td>${repo.getActivite("jeudi", 12,30)}</td>
		<td>${repo.getActivite("vendredi", 12,30)}</td>
	</tr>
	<tr>
		<td>13:00</td>
		<td>${repo.getActivite("lundi", 13,00)}</td>
		<td>${repo.getActivite("mardi", 13,00)}</td>
		<td>${repo.getActivite("mercredi", 13,00)}</td>
		<td>${repo.getActivite("jeudi", 13,00)}</td>
		<td>${repo.getActivite("vendredi", 13,00)}</td>
	</tr>
	<tr>
		<td>13:30</td>
		<td>${repo.getActivite("lundi", 13,30)}</td>
		<td>${repo.getActivite("mardi", 13,30)}</td>
		<td>${repo.getActivite("mercredi", 13,30)}</td>
		<td>${repo.getActivite("jeudi", 13,30)}</td>
		<td>${repo.getActivite("vendredi", 13,30)}</td>
	</tr>
	<tr>
		<td>14:00</td>
		<td>${repo.getActivite("lundi", 14,00)}</td>
		<td>${repo.getActivite("mardi", 14,00)}</td>
		<td>${repo.getActivite("mercredi", 14,00)}</td>
		<td>${repo.getActivite("jeudi", 14,00)}</td>
		<td>${repo.getActivite("vendredi", 14,00)}</td>
	</tr>
	<tr>
		<td>14:30</td>
		<td>${repo.getActivite("lundi", 14,30)}</td>
		<td>${repo.getActivite("mardi", 14,30)}</td>
		<td>${repo.getActivite("mercredi", 14,30)}</td>
		<td>${repo.getActivite("jeudi", 14,30)}</td>
		<td>${repo.getActivite("vendredi", 14,30)}</td>
	</tr>
	<tr>
		<td>15:00</td>
		<td>${repo.getActivite("lundi", 15,00)}</td>
		<td>${repo.getActivite("mardi", 15,00)}</td>
		<td>${repo.getActivite("mercredi", 15,00)}</td>
		<td>${repo.getActivite("jeudi", 15,00)}</td>
		<td>${repo.getActivite("vendredi", 15,00)}</td>
	</tr>
	<tr>
		<td>15:30</td>
		<td>${repo.getActivite("lundi", 15,30)}</td>
		<td>${repo.getActivite("mardi", 15,30)}</td>
		<td>${repo.getActivite("mercredi", 15,30)}</td>
		<td>${repo.getActivite("jeudi", 15,30)}</td>
		<td>${repo.getActivite("vendredi", 15,30)}</td>
	</tr>
	<tr>
		<td>16:00</td>
		<td>${repo.getActivite("lundi", 16,00)}</td>
		<td>${repo.getActivite("mardi", 16,00)}</td>
		<td>${repo.getActivite("mercredi", 16,00)}</td>
		<td>${repo.getActivite("jeudi", 16,00)}</td>
		<td>${repo.getActivite("vendredi", 16,00)}</td>
	</tr>
	<tr>
		<td>16:30</td>
		<td>${repo.getActivite("lundi", 16,30)}</td>
		<td>${repo.getActivite("mardi", 16,30)}</td>
		<td>${repo.getActivite("mercredi", 16,30)}</td>
		<td>${repo.getActivite("jeudi", 16,30)}</td>
		<td>${repo.getActivite("vendredi", 16,30)}</td>
	</tr>
	<tr>
		<td>17:00</td>
		<td>${repo.getActivite("lundi", 17,00)}</td>
		<td>${repo.getActivite("mardi", 17,00)}</td>
		<td>${repo.getActivite("mercredi", 17,00)}</td>
		<td>${repo.getActivite("jeudi", 17,00)}</td>
		<td>${repo.getActivite("vendredi", 17,00)}</td>
	</tr>
	<tr>
		<td>17:30</td>
		<td>${repo.getActivite("lundi", 17,30)}</td>
		<td>${repo.getActivite("mardi", 17,30)}</td>
		<td>${repo.getActivite("mercredi", 17,30)}</td>
		<td>${repo.getActivite("jeudi", 17,30)}</td>
		<td>${repo.getActivite("vendredi", 17,30)}</td>
	</tr>
	<tr>
		<td>18:00</td>
		<td>${repo.getActivite("lundi", 18,00)}</td>
		<td>${repo.getActivite("mardi", 18,00)}</td>
		<td>${repo.getActivite("mercredi", 18,00)}</td>
		<td>${repo.getActivite("jeudi", 18,00)}</td>
		<td>${repo.getActivite("vendredi", 18,00)}</td>
	</tr>
	<tr>
		<td>18:30</td>
		<td>${repo.getActivite("lundi", 18,30)}</td>
		<td>${repo.getActivite("mardi", 18,30)}</td>
		<td>${repo.getActivite("mercredi", 18,30)}</td>
		<td>${repo.getActivite("jeudi", 18,30)}</td>
		<td>${repo.getActivite("vendredi", 18,30)}</td>
	</tr>
	
	
	
</table>


</body>
</html>