DROP TABLE IF EXISTS jour CASCADE;
CREATE TABLE jour(
   id_jour SERIAL,
   nom_jour VARCHAR(50),
   PRIMARY KEY(id_jour)
);

DROP TABLE IF EXISTS heure CASCADE;
CREATE TABLE heure(
   id_heure SERIAL,
   nom_heure TIME,
   PRIMARY KEY(id_heure)
);

DROP TABLE IF EXISTS activite CASCADE;
CREATE TABLE activite(
   id_activite SERIAL,
   nom VARCHAR(50),
   id_jour INT NOT NULL,
   id_heure_debut INT NOT NULL,
   id_heure_fin INT NOT NULL,
   PRIMARY KEY(id_activite),
   FOREIGN KEY(id_jour) REFERENCES jour(id_jour),
   FOREIGN KEY(id_heure_debut) REFERENCES heure(id_heure),
   FOREIGN KEY(id_heure_fin) REFERENCES heure(id_heure)
);

INSERT INTO jour VALUES
(1,'lundi'),(2,'mardi'),
(3,'mercredi'),(4,'jeudi'),
(5,'vendredi');

INSERT INTO heure VALUES
(1,'8:30'),
(2,'9:00'),(3,'9:30'),
(4,'10:00'),(5,'10:30'),
(6,'11:00'),(7,'11:30'),
(8,'12:00'),(9,'12:30'),
(10,'13:00'),(11,'13:30'),
(12,'14:00'),(13,'14:30'),
(14,'15:00'),(15,'15:30'),
(16,'16:00'),(17,'16:30'),
(18,'17:00'),(19,'17:30'),
(20,'18:00'),(21,'18:30'),
(22,'19:00');

select nom,nom_jour,h1.nom_heure as "heure_debut",h2.nom_heure as "heure_fin" from activite
INNER JOIN jour ON activite.id_jour=jour.id_jour
INNER JOIN heure as h1 ON activite.id_heure_debut=h1.id_heure
INNER JOIN heure as h2 ON activite.id_heure_fin=h2.id_heure;

