DROP TABLE IF EXISTS produit;
CREATE TABLE produit(
   id SERIAL,
   nom VARCHAR(50),
   prix DECIMAL(15,2),
   PRIMARY KEY(id)
);

insert into produit(nom,prix) values
	('pomme', 1.5),
	('banane', 5.5),
	('abricot', 1);