<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ajoute produit</title>
<link rel="stylesheet" type="text/css" href="./styles/ajouteProduit.css"/>
</head>
<body>
	<div class="page">
    <form action="ServletAjout" method="POST">

        <div class="input-field">
            <label for="nom">Nom</label>
            <input type="text" name="nom" id="nom" value="${defaultNom}" >
            <div class="erreur">${erreurNom}</div>
        </div>

        <div class="input-field">
            <label for="prix">Prix</label>
            <input type="text" name="prix" id="prix" value="${defaultPrix}" >
            <div class="erreur">${erreurPrix}</div>
        </div>
        
        <div>
        	<input class="button-ajout" type="submit" value="ajouter">
    	</div>

    </form>
</div>
</body>
</html>