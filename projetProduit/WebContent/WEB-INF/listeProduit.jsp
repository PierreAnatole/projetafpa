<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste produits</title>
<link rel="stylesheet" type="text/css" href="./styles/listeProduit.css"/>
</head>
<body>
	<div class="page">
		<h1>Liste produits</h1>
		
		<table>
 			<tr>
		   		<th>id produit</th>
		   		<th>nom</th>
		   		<th>prix</th>
		   		<th></th>
 			</tr>
 	
			<c:forEach var="prod" items="${produits}">
   			<tr>
   				<td><c:out value="${prod.getId()}"/></td>
   				<td><c:out value="${prod.getNom()}"/></td>
   				<td><c:out value="${prod.getPrix()}"/></td>
   				
   				<!-- modifier -->
   				<td class="actions">
   					<form action="ServletUpdate" method="GET">
   						<input type="hidden" name="id" value="<c:out value="${prod.getId()}"/>"></input>
						<input class="button-update" type="submit" value="modifier"></input>
					</form>
				
   				
   				<!-- supprimer -->
   				
   					<form action="ServletDelete" method="POST">
   						<input type="hidden" name="id" value="<c:out value="${prod.getId()}"/>"></input>
						<input class="button-delete" type="submit" value="supprimer"></input>
					</form>
				</td>
				
   			</tr>
			</c:forEach>
		</table>

		
		<form action="ServletAjout" method="GET">
			<input class="button-ajout" type="submit" value="ajouter"></input>
		</form>
	</div>

</body>
</html>