package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnexionPostgres {
    public Connection cnx = null;
    private static ConnexionPostgres instance;
    
    private ConnexionPostgres(){
        try {
        	Configuration conf = new Configuration();
            cnx = DriverManager.getConnection("jdbc:postgresql://localhost:"+conf.getPort()+"/"+conf.getDbName()+"?user="+conf.getUsername()+"&password="+conf.getPassword());
            System.out.println("connexion ok");
        } catch (SQLException ex) {
        	System.out.println("erreur connexion");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
    }

    public Connection getCnx() {
        return cnx;
    }
   
    public static ConnexionPostgres getInstance() {
    	if(instance==null) {
            instance=new ConnexionPostgres();
        }
        return instance;
    }
    
    public static void main (String[] args) {
    	ConnexionPostgres cn= new ConnexionPostgres();//cree une instance qui ouvre une connexion
    	cn.getCnx();
    }


}