package dao;

import java.util.ArrayList;

import entities.Produit;

public interface ProduitDao {
	
	public ArrayList<Produit> getAllProduit();
	
	public Produit getProduitById(int id);
	
	public void deleteProduitById(int id);
	
	public void ajouteProduit(Produit p);
	
	public void updateProduit(int id, String nom, double prix);
}
