package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entities.Produit;

public class ProduitDaoImpl implements ProduitDao{
	ConnexionPostgres cnx = ConnexionPostgres.getInstance();
	Connection dba = cnx.getCnx();
	
	
	/**
	 * recupere la liste de tous les produits dans la base de donnees
	 * @return la liste de tous les produits
	 */
	public ArrayList<Produit> getAllProduit() {
			String query = "SELECT id,nom,prix FROM produit ORDER BY id;";
			ArrayList<Produit> produits = new ArrayList<Produit>();
			try {
				PreparedStatement stmt = dba.prepareStatement(query);
				ResultSet res = stmt.executeQuery();
				while(res.next()) {
					int id=res.getInt("id");
					String nom=res.getString("nom");
					double prenom=res.getDouble("prix");
					
					Produit produit = new Produit(id, nom, prenom);
					produits.add(produit);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return produits;
		}
	
		/**
		 * Recupere le produit qui correspond a  l'id
		 * @param id l'id du produit
		 * @return le produit correspondant
		 */
		public Produit getProduitById(int id) {
			String query = "SELECT id,nom,prix FROM produit WHERE id=?;";
			try {
				PreparedStatement stmt = dba.prepareStatement(query);
				stmt.setInt(1, id);
				ResultSet res = stmt.executeQuery();
				while(res.next()) {
					String nom=res.getString("nom");
					double prenom=res.getDouble("prix");
					
					return new Produit(id, nom, prenom);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		/**
		 * Supprime le produit de l'id correspondant
		 * @param id l'id a supprimer
		 */
		public void deleteProduitById(int id) {
			String query = "DELETE FROM produit WHERE id=?;";
			try {
				PreparedStatement stmt = dba.prepareStatement(query);
				stmt.setInt(1, id);
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * Ajoute un produit
		 * @param e le produit a ajouter
		 */
		public void ajouteProduit(Produit p) {
			String query = "INSERT INTO produit(nom,prix) values (?,?);";
			try {
				PreparedStatement stmt = dba.prepareStatement(query);
				stmt.setString(1, p.getNom());
				stmt.setDouble(2, p.getPrix());
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * Update le produit
		 * @param id l'id du produit a modifier
		 * @param nom le nouveau nom du produit
		 * @param prix le nouveau prix du produit
		 */
		public void updateProduit(int id, String nom, double prix) {
			String query = "UPDATE produit SET (nom,prix) = (?,?) WHERE id = ?";
			try {
				PreparedStatement stmt = dba.prepareStatement(query);
				stmt.setString(1, nom);
				stmt.setDouble(2, prix);
				stmt.setInt(3, id);
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

}

