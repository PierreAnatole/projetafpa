package web;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProduitDaoImpl;
import entities.Produit;

/**
 * Servlet implementation class ServletUpdate
 */
@WebServlet("/ServletUpdate")
public class ServletUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletUpdate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		ProduitDaoImpl pDao = new ProduitDaoImpl();
		Produit produitToUpdate = pDao.getProduitById(id);//on recupere l'id du produit a update
		
		//envoie les informations du produit au jsp updateProduit pour eviter de les retaper
		request.setAttribute("defaultNom", produitToUpdate.getNom());
		request.setAttribute("defaultPrix", produitToUpdate.getPrix());
		request.setAttribute("id", id);//envoie l'id du produit a modifier au jsp
		this.getServletContext().getRequestDispatcher("/WEB-INF/updateProduit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean redirect=false;//passe a true si on doit renvoyer au jsp ajouteProduit
		
		int id = Integer.parseInt(request.getParameter("id"));//recupere l'id de l'loye a modifier
		
		String nom = request.getParameter("nom");
		if(nom.equals("")) {//si le champ nom est vide
			request.setAttribute("erreurNom", "Le champ nom ne doit pas �tre vide");//on envoie l'erreur a l'index
			redirect=true;//on redirige vers l'index
		}
		
		String prixStr = request.getParameter("prix");
		if(prixStr.equals("")) {
			request.setAttribute("erreurPrix", "Le champ prix ne doit pas �tre vide");
			redirect=true;
		} else if (!Pattern.matches("^([0-9]+[.][0-9]{1,2})|([0-9]+)$", prixStr)) {//si l'input ne correspond pas a un nombre (entier ou decimal avec 2 decimales max)
			request.setAttribute("erreurPrix", "Le champ prix doit �tre un nombre decimal (2 decimales max)");
			redirect=true;
		}
		
		if (redirect) {// si on doit rediriger vers ajouteProduit
			request.setAttribute("defaultNom", nom);//envoie les informations deja tapees au jsp ajouteProduit
			request.setAttribute("defaultPrix", prixStr);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajouteProduit.jsp").forward(request, response);
		}

		//les donnees entrees ont etes verifies donc on les convertit dans leurs types respectifs
		double prix = Double.parseDouble(prixStr);
		
		ProduitDaoImpl pDao = new ProduitDaoImpl();
		pDao.updateProduit(id, nom, prix);
		response.sendRedirect("/projetProduit/ServletListe");
	}

}
